﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace WeatherService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class WeatherService : IWeatherService
    {
        public static string ApiKey = "352d0295c5839c726b56";
        public static string ApiUrl = "https://api.weathersource.com/v1/{0}/{1}.json?{2}";

        public List<HistoryResponse> History(string latitudeEq, string longitudeEq, string timestampEq = "", string timestampBetween = "", string period = "day", int limit = 5, int offset = 0, string fields = "temp")
        {
            throw new NotImplementedException();
        }

        public List<HistoryResponse> HistoryByPostalCode(string postalCodeEq, string countryEq = "US", string timestampEq = "", string timestampBetween = "", string period = "day", int limit = 5, int offset = 0, string fields = "temp")
        {
            List<string> parameters = new List<string>();
            parameters.Add(Param("postal_code_eq",postalCodeEq));
            parameters.Add(Param("country_eq",countryEq));
            if(timestampEq != "")
                parameters.Add(Param("timestamp_eq",timestampEq));
            if (timestampBetween != "")
                parameters.Add(Param("timestamp_between",timestampBetween));
            if (period != "")
                parameters.Add(Param("period",period));
            parameters.Add(Param("limit",limit.ToString()));
            parameters.Add(Param("offset",offset.ToString()));
            if (fields != "")
            {
                if(fields == "all")
                {
                    List<String> f = Enum.GetValues(typeof(WeatherField))
                    .Cast<WeatherField>()
                    .Select(v => v.ToString())
                    .ToList();
                    parameters.Add(Param("fields", String.Join(",",f)));
                }
                else
                {
                    parameters.Add(Param("fields", fields));
                }
            }
                

            string url = GenerateApiUrl("history_by_postal_code", parameters);
            string data = GetApiResponse(url);

            List<HistoryResponse> response = new List<HistoryResponse>();
            JToken json = JToken.Parse(data);
            if (json.Type == JTokenType.Array)
            {
                foreach (JObject item in json)
                {
                    HistoryResponse res = HistoryResponse.fromJObject(item);
                    response.Add(res);
                }
            }else
            {
                HistoryResponse res = HistoryResponse.fromJObject((JObject)json);
                response.Add(res);
            }
            return response;
        }

        string Param(string name, string value)
        {
            return name + "=" + value;
        }

        string GenerateApiUrl(string method, List<string> parameters)
        {
            return String.Format(ApiUrl, ApiKey, method, String.Join("&", parameters));
        }

        string GetApiResponse(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                string errorText = "";
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    errorText = reader.ReadToEnd();
                }
                throw new FaultException<WeatherFault>(new WeatherFault(errorText));
            }
        }
    }

}
