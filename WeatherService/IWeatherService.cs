﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Net.Security;
using System.Reflection;

namespace WeatherService
{

    public enum WeatherField
    {
        postal_code,
        country,
        latitude,
        longitude,
        timestamp,
        temp,
        precip,
        snowfall,
        windSpd,
        windDir,
        cldCvr,
        dewPt,
        feelsLike,
        relHum,
        sfcPres,
        spcHum,
        wetBulb,
        tempMax,
        tempAvg,
        tempMin,
        windSpdMax,
        windSpdAvg,
        windSpdMin,
        cldCvrMax,
        cldCvrAvg,
        cldCvrMin,
        dewPtMax,
        dewPtAvg,
        dewPtMin,
        feelsLikeMax,
        feelsLikeAvg,
        feelsLikeMin,
        relHumMax,
        relHumAvg,
        relHumMin,
        sfcPresMax,
        sfcPresAvg,
        sfcPresMin,
        spcHumMax,
        spcHumAvg,
        spcHumMin,
        wetBulbMax,
        wetBulbAvg,
        wetBulbMin
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IWeatherService
    {

        [OperationContract]
        [FaultContractAttribute(typeof(WeatherFault))]
        List<HistoryResponse> History(string latitudeEq, string longitudeEq, string timestampEq = "", string timestampBetween = "", string period = "day", int limit = 5, int offset = 0, string fields = "temp");

        [OperationContract]
        [FaultContractAttribute(typeof(WeatherFault))]
        List<HistoryResponse> HistoryByPostalCode(string postalCodeEq, string countryEq = "US", string timestampEq = "", string timestampBetween = "", string period = "day", int limit = 5, int offset = 0, string fields = "temp");
    }

    [DataContract]
    internal class WeatherFault
    {
        [DataMember]
        string Error { get; set; }

        internal WeatherFault(string error)
        {
            Error = error;
        }
    }

    [DataContract]
    public class HistoryResponse
    {

        string postal_code;
        string country;
        string timestamp;
        decimal? temp;
        decimal? precip;
        decimal? snowfall;
        decimal? windSpd;
        decimal? windDir;
        decimal? cldCvr;
        decimal? dewPt;
        decimal? feelsLike;
        decimal? relHum;
        decimal? sfcPres;
        decimal? spcHum;
        decimal? wetBulb;

        [DataMember]
        public string PostalCode { get { return postal_code; } set { postal_code = value; } }
        [DataMember]
        public string Country { get { return country; } set { country = value; } }
        [DataMember]
        public string Timestamp { get { return timestamp; } set { timestamp = value; } }
        [DataMember]
        public decimal? Temperature { get { return temp; } set { temp = value; } }
        [DataMember]
        public decimal? Precipitation { get { return precip; } set { precip = value; } }
        [DataMember]
        public decimal? Snowfall { get { return snowfall; } set { snowfall = value; } }
        [DataMember]
        public decimal? WindSpeed { get { return windSpd; } set { windSpd = value; } }
        [DataMember]
        public decimal? WindDirection { get { return windDir; } set { windDir = value; } }
        [DataMember]
        public decimal? CloudCover { get { return cldCvr; } set { cldCvr = value; } }
        [DataMember]
        public decimal? DewPoint { get { return dewPt; } set { dewPt = value; } }
        [DataMember]
        public decimal? FeelsLike { get { return feelsLike; } set { feelsLike = value; } }
        [DataMember]
        public decimal? RelativeHumidity { get { return relHum; } set { relHum = value; } }
        [DataMember]
        public decimal? SurfacePressue { get { return sfcPres; } set { sfcPres = value; } }
        [DataMember]
        public decimal? SpecificHumidity { get { return spcHum; } set { spcHum = value; } }
        [DataMember]
        public decimal? WetBulbTemperature { get { return wetBulb; } set { wetBulb = value; } }

        internal static HistoryResponse fromJObject(JObject item)
        {
            HistoryResponse me = new HistoryResponse();
            try
            {
                foreach (JProperty p in item.Properties())
                {
                    string field = p.Name;
                    var value = p.Value;
                    var myField = me.GetType().GetField(field, BindingFlags.NonPublic|BindingFlags.Instance);
                    if (myField != null && value != null && value.Type != JTokenType.Null)
                    {
                        Type t = Nullable.GetUnderlyingType(myField.FieldType) ?? myField.FieldType;
                        object safeValue = (p == null) ? null : Convert.ChangeType(value, t);
                        myField.SetValue(me, safeValue);
                    }
                }
            }catch (Exception ex)
            {
                throw new FaultException<WeatherFault>(new WeatherFault(ex.Message));
            }
            return me;
        }

    }
}
